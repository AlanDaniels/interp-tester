
package com.alandaniels.interp.desktop;


import java.awt.Dimension;
import java.awt.Toolkit;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.alandaniels.interp.InterpTesterApp;


public class DesktopLauncher {

    /**
     * And away we go.
     * LibGDX doesn't have a graceful way to get the desktop size before
     * the app is initialized, so use AWT instead. Go for the default
     * monitor size, running full-screen.
     */
	public static void main (String[] arg) {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title  = "Alan's Interpolation Tester";
		config.width  = dim.width;
		config.height = dim.height;
		config.fullscreen = true;
		new LwjglApplication(new InterpTesterApp(), config);
	}
}
