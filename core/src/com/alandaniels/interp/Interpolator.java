package com.alandaniels.interp;

public class Interpolator {

    public static double Linear(double val) {
        if (val < 0.0) {
            return 0.0;
        }
        else if (val > 1.0) {
            return 1.0;
        }
        else {
            return val;
        }
    }
}
