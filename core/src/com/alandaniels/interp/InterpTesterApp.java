package com.alandaniels.interp;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;


public class InterpTesterApp extends ApplicationAdapter {

	private SpriteBatch batch;
	private Texture img;
	private Camera camera;
	private Viewport viewport;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");

		// Use the default screen size, minus 100 pixels on each side.
        Graphics.DisplayMode mode = Gdx.graphics.getDisplayMode();
        int width  = mode.width;
        int height = mode.height;
        Gdx.app.log("Alan", String.format("Original window size is %d x %d", width, height));

        camera = new OrthographicCamera(width, height);
        camera.position.set(width / 2.0f, height / 2.0f, 0.0f);
        viewport = new FitViewport(width, height, camera);
    }

    @Override
    public void resize(int width, int height) {
        Gdx.app.log("Alan", String.format("Resizing to %d x %d", width, height));

        camera.viewportWidth  = width;
        camera.viewportHeight = height;
        camera.position.set(width / 2.0f, height / 2.0f, 0.0f);
	    camera.update();

        viewport.update(width, height);
    }

    /**
     * Render. Paint a dark blue, then other stuff.
     */
	@Override
	public void render () {
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.2f, 1.0f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		camera.update();
		batch.setProjectionMatrix(camera.combined);

		batch.begin();
		batch.draw(img, 0, 0);
		batch.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
	}
}
